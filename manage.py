#!venv/bin/python
import sys

from flask_migrate import MigrateCommand
from flask_script import Server, Manager

from webapp import create_app

mode = 'development'
if len(sys.argv) == 3 and sys.argv[1] == 'runserver' and sys.argv[2] == 'production':
    mode = 'production'
    del sys.argv[2]
    print 'prod!!'

app = create_app(mode)

manager = Manager(app)
manager.add_command("runserver", Server(host="0.0.0.0"))
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()
