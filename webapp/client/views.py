from webapp.client import client
from flask import render_template


@client.route("/")
def index():
    return render_template("index.html")
