from flask import Flask, redirect, render_template
from flask_blogging import SQLAStorage, BloggingEngine
from flask_security import Security, SQLAlchemyUserDatastore
from markdown.extensions.codehilite import CodeHiliteExtension
from sqlalchemy import create_engine, MetaData

import sensitive


def _init_security(app, db):
    from store.models import User, Role

    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    security.init_app(app, user_datastore)


def create_app(option):
    app = Flask(__name__)
    app.config["SECRET_KEY"] = sensitive.SECRET_KEY
    app.config["BLOGGING_URL_PREFIX"] = "/blog"
    app.config["BLOGGING_DISQUS_SITENAME"] = None
    app.config["BLOGGING_SITEURL"] = "http://localhost:8008"
    app.config["BLOGGING_SITENAME"] = "PiSquared Blog"
    app.config["FILEUPLOAD_IMG_FOLDER"] = "fileupload"
    app.config["FILEUPLOAD_PREFIX"] = "/fileupload"
    app.config["FILEUPLOAD_ALLOWED_EXTENSIONS"] = ["png", "jpg", "jpeg", "gif"]
    return app


engine = create_engine('postgresql://blog_user:%s@localhost:5432/blog' % sensitive.ADMIN_PASSWORD)
meta = MetaData()
sql_storage = SQLAStorage(engine, metadata=meta)
code_highlight_ext = CodeHiliteExtension({})
blog_engine = BloggingEngine(app, sql_storage, extensions=[code_highlight_ext])
meta.create_all(bind=engine)
security = Security(app)

